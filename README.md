# QCourse511-1 Project Phase 2021 - Amir Khan 

## Platform Used to build Project

- [IBM Quantum Lab](https://quantum-computing.ibm.com/lab)

![Lab Image](ibmquantumlabinterface.png)

## Project status

### Mid Deliverables

#### Steps Taken - Code development

- Imports

General Imports

Visualisation Imports

Scikit Imports

Qiskit Imports

- Data Split

Load mnist digits dataset using sklearn.datasets

Plot example '0' and '1' and '2'

MNIST Digit Labels Taken - 0, 1, 2

Split dataset

train is now 75% of the entire data set

validation is now 15% of the initial data set

test is now 10% of the initial data set

- Data Standardization and Normalization

1. Standardize
2. Reduce dimensions - PCA
3. Normalize - MinMax Scaler


#### Quantum Data Encoding, Machine Learning Modelling and Accuracy Findings

using sklearn SVM with 'precomputed' kernel

##### ZZFeatureMap without Entanglement

Label 0 vs rest on Validation Set Accuracy- 97.53086419753086%

Label 1 vs rest on Validation Set Accuracy - 97.53086419753086%

Label 2 vs rest on Validation Set Accuracy - 92.5925925925926%

Overall Accuracy on Test Set Accuracy - 0.9629629629629629 (96.29%)

##### ZZFeatureMap with Entanglement - Linear

Label 0 vs rest on Validation Set Accuracy - 96.29629629629629%

Label 1 vs rest on Validation Set Accuracy - 91.35802469135803%

Label 2 vs rest on Validation Set Accuracy - 96.29629629629629%

Overall Accuracy on Test Set Accuracy - 0.9444444444444444 (94.44%)

##### ZZFeatureMap with Entaglement - Circular

Label 0 vs rest on Validation Set Accuracy - 96.29629629629629%

Label 1 vs rest on Validation Set Accuracy - 96.29629629629629%

Label 2 vs rest on Validation Set Accuracy- 97.53086419753086%

Overall Accuracy on Test Set Accuracy - 0.9814814814814815 (98.14%)

##### PauliFeatureMap

Label 0 vs rest on Validation Set Accuracy - 100.0%

Label 1 vs rest on Validation Set Accuracy - 98.76543209876543%

Label 2 vs rest on Validation Set Accuracy - 97.53086419753086%

Overall Accuracy on Test Set Accuracy - 0.9629629629629629 (96.29%)

##### Accuracy Comparison Graph

ZZFeatureMap without Entaglement - 96.29%

ZZFeatureMap with Entaglement - Linear - 94.44%

ZZFeatureMap with Entaglement - Circular - 98.14%

PauliFeatureMap -  96.29%

### Final Deliverables

Graphical Visualization embedded in final notebook of Probability Distributions of Confidence Scores vs Labels on FeatureMap Kernels.

Graphical Visualization embedded in final notebook of Probability Scores of classical SVC (Support Vector Classifier) kernels on FeatureMap Kernels Encoded Fitted on Training Data and Evaluated on Validation Data

Graphical Visualization embedded in last cell of final notebook of Overall Accuracy Scores Evaluated on FeatureMaps (ZZFeatureMap without Entaglement, ZZFeatureMap with Entaglement - Linear, ZZFeatureMap with Entaglement - Circular, PauliFeatureMap) on Multiclass Labels (0,1,2) on MNIST Data.

Final Completed Jupyter Notebook

## License
Apache License 2.0